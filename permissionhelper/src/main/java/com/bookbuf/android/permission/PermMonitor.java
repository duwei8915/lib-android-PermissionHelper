package com.bookbuf.android.permission;

import android.util.Log;

import com.bookbuf.android.permission.entity.MonitorActivityEntity;
import com.bookbuf.android.permission.entity.PermEntityCompat;

import java.util.Arrays;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Map;

/**
 * 权限注册中心 (监视器)
 */
public class PermMonitor {

	private static final String TAG = "PermMonitor";
	private static LinkedList<MonitorActivityEntity> linkedList = new LinkedList<> ();
	private static IncreaseHelper increaseHelper = new IncreaseHelper ();


	public static void print (String TAG) {
		for (MonitorActivityEntity entity : linkedList) {
			Log.d (TAG, "print: " + entity.toString ());
		}
	}

	public static boolean isNotEmpty () {
		return linkedList.size () == 1;
	}

	public static void addOrUpdate (MonitorActivityEntity entity) {
		if (isNotEmpty ()) {
			clear ();
			addLast (entity);
		} else {
			addLast (entity);
		}
		print ("addOrUpdate");
	}

	public static void clear () {
		linkedList.clear ();
		Log.d (TAG, "clear: ");
	}

	public static MonitorActivityEntity get () {
		print ("get");
		if (isNotEmpty ()) {
			return peekLast ();
		} else {
			return null;
		}
	}

	/*添加到 linklist 末尾*/
	private static void addLast (MonitorActivityEntity entity) {
		if (!entity.isIdValid ()) {
			int monitorId = increaseHelper.obtainId ();
			entity.setId (monitorId);
			linkedList.addLast (entity);
		} else {
			int index = linkedList.indexOf (entity);
			if (index == -1) {
				linkedList.addLast (entity);
			}
		}
		Log.d (TAG, "addLast: monitorId = " + entity.getId ());
	}

	/*弹出 最后一个项*/
	private static MonitorActivityEntity pollLast () {
		return linkedList.pollLast ();
	}

	/*选择 最后一个项*/
	private static MonitorActivityEntity peekLast () {
		return linkedList.peekLast ();
	}

	public static final class Box {
		private static final String TAG = "Box";
		private static IncreaseHelper increaseHelper = new IncreaseHelper ();
		private static Hashtable<Integer, Object> objects = new Hashtable<> ();

		public static <Permission extends PermEntityCompat> int put (Permission[] permission) {
			int id = increaseHelper.obtainId ();
			objects.put (id, permission);
			Log.d (TAG, "put: " + id + "=" + Arrays.toString (PermUtil.permissions (permission)));
			return id;
		}

		public static <Permission extends PermEntityCompat> Permission[] filter (int id) {
			if (objects.containsKey (id)) {
				return (Permission[]) objects.get (id);
			}
			return null;
		}

		public static <Permission extends PermEntityCompat> int filter (Permission[] permission) {
			for (Map.Entry<Integer, Object> entry : objects.entrySet ()) {
				if (entry.getValue ().equals (permission)) {
					return entry.getKey ();
				}
			}
			return -1;
		}

		public static <Permission extends PermEntityCompat> void remove (int id) {
			if (objects.containsKey (id)) {
				Permission[] permission = (Permission[]) objects.remove (id);
				Log.d (TAG, "remove: " + id + "=" + Arrays.toString (PermUtil.permissions (permission)));
			}
		}

		public static void clear () {
			objects.clear ();
			Log.d (TAG, "clear: ");
		}

		public static void print () {
			for (Map.Entry<Integer, Object> entry : objects.entrySet ()) {
				Object obj = entry.getValue ();
				if (obj instanceof PermEntityCompat[]) {
					PermEntityCompat[] perms = (PermEntityCompat[]) obj;
					Log.d (TAG, "print: " + Arrays.toString (PermUtil.permissions (perms)));
				} else if (obj instanceof PermEntityCompat[]) {
					PermEntityCompat[] perm23s = (PermEntityCompat[]) obj;
					Log.d (TAG, "print: " + Arrays.toString (PermUtil.permissions (perm23s)));
				} else {
					throw new IllegalArgumentException ("type must be PermEntityCompat or PermEntityCompat");
				}
			}
		}

	}

}
