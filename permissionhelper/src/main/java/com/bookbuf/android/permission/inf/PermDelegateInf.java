package com.bookbuf.android.permission.inf;

import android.app.Activity;

import com.bookbuf.android.permission.entity.PermEntityCompat;

/**
 * 权限模板方法 通常权限申请是在单个容器中 包含不限于 activity/service等.
 */
public interface PermDelegateInf<Permission extends PermEntityCompat> {

	/*为容器注册回调*/
	void register (Activity activity, OnRequestPermissionsResultCallback<Permission> callback);

	/*跟踪请求*/
	void trace (Permission... permissions);

	/*请求权限*/
	void request (Permission... permissions);

	/*检查权限*/
	boolean check (Permission... permissions);

	/*解析权限*/
	void parse (int traceId, String[] strings, int[] grants);

	/*取消跟踪请求*/
	void untrace (int traceId);

	/*为容器取消注册*/
	void unregister (Activity activity, OnRequestPermissionsResultCallback<Permission> callback);
}
