package com.bookbuf.android.permission.entity;

public class PermEntityCompat extends PermEntity {

	/**
	 * 请求权限使用的标题
	 */
	protected String title;
	/**
	 * 请求权限使用的说明内容
	 */
	protected String content;

	public PermEntityCompat (String permission, String title, String content) {
		super (permission);
		this.title = title;
		this.content = content;
	}

	public String getTitle () {
		return title;
	}

	public void setTitle (String title) {
		this.title = title;
	}

	public String getContent () {
		return content;
	}

	public void setContent (String content) {
		this.content = content;
	}
}
