package com.bookbuf.android.permission.inf;

/** 封装 {@link com.bookbuf.android.permission.delegate.PermDelegateCompat#onRequestPermissionsResult(int, String[], int[])} 以便于在客户端清晰的知道请求的几种处理结果 */
public interface OnRequestPermissionsResultCallback<Permission> {

	/*申请权限--授予*/
	void onGranted (Permission... permission);

	/*申请权限--拒绝*/
	void onDenied (Permission... permission);

	/*检查权限*/
	void onChecked (boolean isGranted, Permission... permission);

}