package com.bookbuf.android;

import android.Manifest;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.bookbuf.android.permission.PermMonitor;
import com.bookbuf.android.permission.PermUtil;
import com.bookbuf.android.permission.delegate.PermDelegateCompat;
import com.bookbuf.android.permission.entity.PermEntityCompat;
import com.bookbuf.android.permission.inf.OnRequestPermissionsResultCallback;

import java.util.Arrays;

public class MainActivity extends AppCompatActivity {

	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate (savedInstanceState);
		setContentView (R.layout.activity_main);
	}

	private final String TAG = "Client";
	private OnRequestPermissionsResultCallback<?> callback;

	/*示例权限*/
	public PermEntityCompat[] exampleSinglePerm () {
		PermEntityCompat perm = new PermEntityCompat (Manifest.permission.READ_CONTACTS, "联系人权限", "读取联系人权限是为了xxxxxx");
		return new PermEntityCompat[]{perm};
	}

	public PermEntityCompat[] exampleGroupPerms () {
		PermEntityCompat[] perms = new PermEntityCompat[]{
				new PermEntityCompat (Manifest.permission.READ_CONTACTS, "联系人权限", "读取联系人权限是为了xxxxxx"),
				new PermEntityCompat (Manifest.permission.READ_CALENDAR, "日历权限", "读取日历权限是为了 xxxxxx"),
				new PermEntityCompat (Manifest.permission.READ_SMS, "短信权限", " 读取短信权限是为了 xxxxxx")
		};
		return perms;
	}

	public OnRequestPermissionsResultCallback callback () {
		if (callback != null) return callback;
		callback = new OnRequestPermissionsResultCallback<PermEntityCompat> () {
			@Override
			public void onGranted (PermEntityCompat... permission) {
				String[] perms = PermUtil.permissions (permission);
				Log.e (TAG, "onGranted : " + Arrays.toString (perms));
			}

			@Override
			public void onDenied (PermEntityCompat... permission) {
				String[] perms = PermUtil.permissions (permission);
				Log.e (TAG, "onDenied : " + Arrays.toString (perms));
			}

			@Override
			public void onChecked (boolean isGranted, PermEntityCompat... permission) {
				String[] perms = PermUtil.permissions (permission);
				Log.e (TAG, "onChecked : " + Arrays.toString (perms) + ":权限检查:" + isGranted);
			}
		};
		return callback;
	}

	public void onCallbackRegister (View view) {
		PermDelegateCompat.Client.register (this, callback ());
	}

	public void onSinglePermRequest (View view) {
		PermDelegateCompat.Client.request (exampleSinglePerm ());
	}

	public void onMultiPermRequest (View view) {
		PermDelegateCompat.Client.request (exampleGroupPerms ());
	}

	public void onSinglePermCheck (View view) {
		PermDelegateCompat.Client.check (exampleSinglePerm ());
	}

	public void onMultiPermCheck (View view) {
		PermDelegateCompat.Client.check (exampleGroupPerms ());
	}

	public void onRegisteredCallbackNumberDisplay (View view) {
		PermMonitor.print (TAG);
	}

	public void onCallbackUnregister (View view) {
		PermDelegateCompat.Client.unregister (this, callback ());
	}


	public void onPrintBox (View view) {
		PermMonitor.Box.print ();
	}

	public void onPrintPermMonitor (View view) {
		PermMonitor.print ("MainActivity");
	}

	public void onApplyPermission (View view) {
		PermDelegateCompat.Client.requestIfNotAcquirePermission (exampleGroupPerms ());
	}

	@Override
	public void onRequestPermissionsResult (int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		super.onRequestPermissionsResult (requestCode, permissions, grantResults);
		PermDelegateCompat.onRequestPermissionsResult (requestCode, permissions, grantResults);
	}
}
